package br.com.endcraft.fight;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerState {

	private Location lastPos;
	
	private final Player player;
	public PlayerState(Player player) {
		this.player = player;
	}
	
	
	public void saveLastPos() {
		this.lastPos = player.getLocation().clone();
	}
	
	
	public Location restoreLastPos() {
		return lastPos;
	}
	
	
	public Player getPlayer() {
		return player;
	}
	
}
