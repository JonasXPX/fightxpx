package br.com.endcraft.fight;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.meta.ItemMeta;


public class PlayerListeners implements Listener{

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if(e.getEntity() != null) {
			try {
				Game game = Fight.getGameByPlayer(e.getEntity().getName());
				if(game == null) {
					return;
				}
				Fight.log("Called event on game " + game.getPlayers().toString());
				if(game.isFinalizado()) {
					return;
				}
				if(!game.isPrepare() && !game.isIniciado()) {
					return;
				}
				e.getDrops().forEach(d -> d.setType(Material.AIR));
				game.killPlayer(e.getEntity());
			}catch (NullPointerException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	@EventHandler
	public void onHitPlayers(EntityDamageByEntityEvent e) {
		if(e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player damager = (Player)e.getDamager();
			Player player = (Player)e.getEntity();
			Game game = Fight.getGameByPlayer(damager.getName());
			Game user2 = Fight.getGameByPlayer(player.getName());
			if(game != null && user2 != null) {
				if(game.equals(user2)) {
					if(game.isPrepare()) {
						damager.sendMessage("�cAguarde o evento iniciar");
						e.setCancelled(true);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void kitSelectionEvent(InventoryClickEvent e) {
		if(e.getInventory().getName().equals("�c�lSELECIONE SEU KIT")) {
			e.setCancelled(true);
			int slot = e.getSlot();
			Fight.getKits().get(slot).put((Player)e.getWhoClicked());
		}
	}
	
	
	@EventHandler
	public void onOpenKitSelector(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getPlayer().getItemInHand().getType() == Material.CHEST) {
				ItemMeta meta = e.getPlayer().getItemInHand().getItemMeta();
				if(!meta.hasDisplayName()) {
					return;
				}
				if(meta.getDisplayName().equals("�c�lSELECIONE SEU KIT")) {
					e.setCancelled(true);
					e.getPlayer().openInventory(new InventoryKit(e.getPlayer()));	
				}
			}
		}
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		Game game = Fight.getGameByPlayer(e.getPlayer().getName());
		if(game == null)
			return;
		if(game.isPrepare()) {
			if(e.isCancelled()) {
				e.setCancelled(false);
				Fight.log("Jogador foi for�ado para teleporte: " + e.getPlayer().getName());
			}
		}
		if(game.isFinalizado()) {
			if(e.isCancelled()) {
				e.setCancelled(false);
				Fight.log("Jogador foi for�ado para teleporte: " + e.getPlayer().getName());
			}
		}
		if(game.isIniciado() || game.isPrepare()) {
			if(e.getCause() == TeleportCause.COMMAND) {
				e.getPlayer().sendMessage("�7[Ultra PVP] �cVoc� n�o pode sair do evento agora.");
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		try {
			Fight.log("Called event on game");
			Game game = Fight.getGameByPlayer(e.getPlayer().getName());
			if(game == null) {
				return;
			}
			if(game.isFinalizado()) {
				return;
			}
			if(!game.isIniciado() && !game.isPrepare()) {
				game.exitQuery(e.getPlayer());
				return;
			}
			if(game.isPrepare() || game.isIniciado()) {
				e.getPlayer().teleport(Fight.outLocation);
			}
			game.killPlayer(e.getPlayer());
		}catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}
	
	@EventHandler
	public void onCommandExecute(PlayerCommandPreprocessEvent e) {
		if(Fight.isInBlockCommands(e.getMessage().replaceAll("/", ""))) {
			Game game = Fight.getGameByPlayer(e.getPlayer().getName());
			if(game == null)
				return;
			e.getPlayer().sendMessage("�cVoc� n�o pode jogar 2 jogos ao mesmo tempo.");
			e.setCancelled(true);
		}
	}
}
