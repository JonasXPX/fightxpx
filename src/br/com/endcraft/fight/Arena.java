package br.com.endcraft.fight;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

public class Arena implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1663490888068018991L;
	public final static int MAX_ARENA_GAMES = 1;
	public int size = 0;
	private final String name;
	public List<SpawnPoint> spawns;
	
	public Arena(String name) {
		this.name = name;
		spawns = Lists.newArrayList();
	}
	
	public List<SpawnPoint> getSpawns() {
		return spawns;
	}
	
	public String getName() {
		return name;
	}
	
	
	
	
}
