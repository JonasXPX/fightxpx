package br.com.endcraft.fight;

import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class SpawnPoint implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9092388803502819149L;
	private final double x,y,z;
	private final float yaw,pitch;
	private final String world;
	
	public SpawnPoint(String world, double x, double y, double z, float yaw, float pitch) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.pitch = pitch;
		this.yaw = yaw;
		this.world = world;
	}
	
	
	
	public static SpawnPoint toSpawnPoint(Location loc) {
		return new SpawnPoint(loc.getWorld().getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
	}
	
	public static Location toLocation(SpawnPoint sp) {
		return new Location(Bukkit.getWorld(sp.getWorld()), sp.getX(), sp.getY(), sp.getZ(), sp.getYaw(), sp.getPitch());
	}

	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}


	public double getZ() {
		return z;
	}


	public float getPitch() {
		return pitch;
	}


	public float getYaw() {
		return yaw;
	}


	public String getWorld() {
		return world;
	}
	
}
