package br.com.endcraft.fight;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryKit extends InventoryView{

	private Player player;
	private Inventory inv;
	public InventoryKit(Player player) {
		setPlayer(player);
		inv = Bukkit.createInventory(getPlayer(), 54, "�c�lSELECIONE SEU KIT");
		Fight.getKits().forEach(kit -> {
			ItemStack item = new ItemStack(Material.CHEST);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', kit.getName()));
			item.setItemMeta(meta);
			inv.addItem(item);
		});
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Override
	public Inventory getBottomInventory() {
		return getPlayer().getInventory();
	}

	@Override
	public HumanEntity getPlayer() {
		return player;
	}

	@Override
	public Inventory getTopInventory() {
		return inv;
	}

	@Override
	public InventoryType getType() {
		return InventoryType.CHEST;
	}

	
}
