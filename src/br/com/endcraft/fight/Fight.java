package br.com.endcraft.fight;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.json.JSONObject;

import com.google.common.collect.Lists;

public class Fight extends JavaPlugin{

	private static List<Game> games;
	private static List<Arena> arenas;
	private static List<PlayerState> states;
	private static List<Kit> kits;
	private static Fight instance;
	public static Scoreboard scoreboard;
	public static Location outLocation;
	public static List<String> blockCommands;
	
	public static final String ARENA_FILE ="arenas.dat";
	
	@Override
	public void onEnable() {
		instance = this;

		if(getConfig().contains("saida")) {
			String[] out = getConfig().getString("saida").split(";");
			outLocation = new Location(getServer().getWorld(out[0]),
					Double.parseDouble(out[1]), Double.parseDouble(out[2]), Double.parseDouble(out[3]));
		}
		scoreboard = getServer().getScoreboardManager().getMainScoreboard();
		games = new ArrayList<>();
		arenas = new ArrayList<>();
		states = new ArrayList<>();
		kits = loadKits();
		
		File fileArenas = new File(getDataFolder(), ARENA_FILE);
		if(fileArenas.exists()) {
			try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileArenas))){
				Object obj = ois.readObject();
				if(obj instanceof List<?>) {
					arenas = (List<Arena>) obj;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		getCommand("pvp").setExecutor(new Comandos());
		
		getServer().getPluginManager().registerEvents(new PlayerListeners(), this);
		
		if(!getConfig().contains("block_commands")) {
			getConfig().set("block_commands", Arrays.asList("fight", "evento", "gladiador", "killer"));
			saveConfig();
		}
		blockCommands = getConfig().getStringList("block_commands");
	}
	
	
	public static Game getGameByPlayer(String player) {
		for(Game g : games) {
			if(g.getPlayers().contains(player))
				return g;
		}
		return null;
	}
	
	
	public static Scoreboard getScoreboard() {
		return scoreboard;
	}
	
	public static List<Game> getGames() {
		return games;
	}

	public static Fight getInstance() {
		return instance;
	}
	
	
	public static List<Arena> getArenas(){
		return arenas;
	}
	
	public static Arena getArenaByName(String name) {
		for(Arena a : getArenas()) {
			if(a.getName().equalsIgnoreCase(name))
				return a;
		}
		return null;
	}
	
	@Override
	public void onDisable() {
		for(Team t : getScoreboard().getTeams()) {
			if(t.getName().startsWith("game-")) {
				getLogger().log(Level.INFO, "Time: " + t.getName() + " was unregistred.");
				t.unregister();
			}
		}
		File folder = getDataFolder();
		if(!folder.exists()) {
			folder.mkdir();
		}
		saveKits();
		arenas.forEach(p -> p.size = 0);
		File fileArena = new File(getDataFolder(), ARENA_FILE);
		if(!arenas.isEmpty()) {
			try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileArena))){
				oos.writeObject(arenas);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void log(String msg) {
		instance.getLogger().log(Level.INFO, msg);
	}
	
	
	public static PlayerState getStateByName(String player) {
		for(PlayerState state : states) {
			if(state.getPlayer().getName().equalsIgnoreCase(player)) {
				return state;
			}
		}
		return null;
	}
	
	public static List<PlayerState> getStates() {
		return states;
	}
	public static List<Kit> getKits() {
		return kits;
	}


	public static void clear() {
		List<Game> toRemove = Lists.newArrayList();
		for(Game games : getGames()) {
			if(games.isFinalizado()) {
				toRemove.add(games);
			}
		}
		
		toRemove.forEach(g -> getGames().remove(g));
	}
	
	public List<Kit> loadKits(){
		if(!getDataFolder().exists()) {
			getDataFolder().mkdir();
			return Lists.newArrayList();
		}
		List<Kit> kits = new ArrayList<>();
		File json = new File(getDataFolder(), "kits.json");
		if(!json.exists()) {
			return Lists.newArrayList();
		}
		List<String> data = null;
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(json))){
			data = (List<String>) ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		data.forEach(j -> {
			kits.add(Kit.fromJson(new JSONObject(j)));
		});
		return kits;
	}
	
	public void saveKits() {
		if(!getDataFolder().exists()) {
			getDataFolder().mkdir();
		}
		List<String> data = Lists.newArrayList();
		kits.forEach(k -> data.add(Kit.toJson(k).toString()));
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(getDataFolder(), "kits.json")))){
			oos.writeObject(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isInBlockCommands(String startWith) {
		for(String s : blockCommands) {
			if(s.startsWith(startWith))
				return true;
		}
		return false;
	}
}
