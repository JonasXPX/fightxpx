package br.com.endcraft.fight;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Tools {

	
	public static boolean checkClearInventory(Player player) {
		for(ItemStack i : player.getInventory().getArmorContents()) {
			if(i != null && i.getType() != Material.AIR) {
				Fight.log("Found it :" + i.getType().name());
				return false;
			}
		}
		for(ItemStack i : player.getInventory().getContents()) {
			if(i != null && i.getType() != Material.AIR) {
				Fight.log("Found it :" + i.getType().name());
				return false;
			}
		}
		return true;
	}
}
