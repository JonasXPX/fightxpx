package br.com.endcraft.fight;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Team;

import com.google.common.collect.Lists;

public class Game {
	
	public final static int MAX_PLAYERS = 2;

	public static final ItemStack bau = new ItemStack(Material.CHEST);

	private static final int delay = 30;

	private Fight plugin; // Inst�ncia
	
	private int quantidade = 0;
	private List<String> startPlayers;
	private List<String> players;
	private Arena arena;
	private BukkitTask waitTask;
	private Team time;
	private boolean iniciado = false;
	private boolean finalizado = false;
	private boolean prepare = false;
	private long tempoDeJogo = 0;
	private int countDown = 0;
	
	private Random rd = new Random();
	
	
	public Game(Fight plugin, Arena arena) {
		time = Fight.getScoreboard().registerNewTeam("game-" + rd.nextInt(1000));
		time.setAllowFriendlyFire(false);
		this.plugin = plugin;
		this.arena = arena;
		ItemMeta itemMeta = bau.getItemMeta();
		itemMeta.setDisplayName("�c�lSELECIONE SEU KIT");
		bau.setItemMeta(itemMeta);
		players = Lists.newArrayList();
		startPlayers = Lists.newArrayList();
		arena.size++;
		waitTask = new BukkitRunnable() {
			@Override
			public void run() {
				sendMessageForAll("�7[Ultra PVP] �aAguardando mais jogadores... " + quantidade + "/" + MAX_PLAYERS);
			}
		}.runTaskTimer(plugin, 0, 20 * 10);
	}
	
	public List<String> getPlayers() {
		return players;
	}

	public void killPlayer(Player p) {
		int index = players.indexOf(p.getName());
		players.remove(index);
		time.removePlayer(p);
		startPlayers.add(p.getName());
		Fight.log("Size players: " + players.size());
		clearContents(p);
		if(players.size() == 1 && !isPrepare()) {
			finishGame();
		}
	}
	
	
	public int getQuantidade() {
		return quantidade;
	}

	public void entryQuery(Player sender) {
		players.add(sender.getName());
		time.addPlayer(sender);
		quantidade++;
		sender.playSound(sender.getLocation(), Sound.CLICK, 1F, 2F);
		sender.sendMessage("�7[Ultra PVP] �aVoc� entrou! aguarde...");
		if(quantidade == MAX_PLAYERS) {
			prepareGame();
		}
	}
	
	public void exitQuery(Player player) {
		if(!iniciado) {
			players.remove(player.getName());
			time.removePlayer(player);
			quantidade--;
			player.sendMessage("�7[Ultra PVP] �aVoc� saiu da fila de espera.");
			player.sendMessage("�7[Ultra PVP] �aN�o fique sem PVP chame seu amigos!");
		}
	}

	private void prepareGame() {
		List<String> toRemove = Lists.newArrayList();
		for(String p : players) {
			Player player = Bukkit.getPlayer(p);
			if(player == null) {
				quantidade--;
				toRemove.add(p);
				Fight.log("Player is null \"" + p + "\"");
			}
			if(!Tools.checkClearInventory(player)) {
				player.sendMessage("�7[Ultra PVP] �cLimpe seu invent�rio antes! e digite novamente para entrar.");
				quantidade--;
				toRemove.add(p);
				Fight.log("Player com invent�rio ocupado \"" + p + "\"");
			}
		}
		toRemove.forEach( p -> players.remove(p));
		if(!toRemove.isEmpty())
			return;
		prepare = true;
		waitTask.cancel();
		
		sendMessageForAll("�7[Ultra PVP] �aIniciando jogo em " + delay + " segundos");
		Fight.log("Jogadores na arena: " + players.toString());
		playSoundForAll(Sound.EXPLODE);
		
		/* Teleport all players randomly */
		getPlayers().forEach(st -> {
			Player player = Bukkit.getPlayer(st);
			int rnd = rd.nextInt(arena.getSpawns().size());
			Location sp = SpawnPoint.toLocation(arena.getSpawns().get(rnd));
			player.teleport(sp);
			if(!Fight.getKits().isEmpty()) {
				player.getInventory().setItem(4, bau);
			} else 
				Kit.addKit(player);
		});
		
		
		new BukkitRunnable() {
			@Override
			public void run() {
				startGame();
			}
		}.runTaskLater(plugin, 20 * delay);
		new BukkitRunnable() {
			
			@Override
			public void run() {
				countDown++;
				if(countDown > (delay - 5)) {
					playSoundForAll(Sound.ARROW_HIT);
					sendMessageForAll("�7[Ultra PVP] �cIniciando PVP em " + ((delay - countDown) + 1));
					if(countDown == delay)
						this.cancel();
				}
			}
		}.runTaskTimer(plugin, 0, 20);
	}

	private void startGame() {
		sendMessageForAll("�7[Ultra PVP] �b�lJogo iniciado!");
		playSoundForAll(Sound.AMBIENCE_THUNDER);
		iniciado = true;
		prepare = false;
		time.setAllowFriendlyFire(true);
		tempoDeJogo = System.currentTimeMillis();
		managerKits();
		if(players.size() == 1) {
			finishGame();
		}
	}
	
	/*
	 * Adicionar o kit padr�o caso o jogador n�o escolha
	 * nenhum kit
	 */
	private void managerKits() {
		getPlayers().forEach(p -> {
			Player player = Bukkit.getPlayer(p);
			for(ItemStack item : player.getInventory().getContents()) {
				if(item != null && item.getType() == bau.getType()) {
					item.setType(Material.AIR);
					Fight.getKits().get(0).put(player);
				}
			}
		});
	}

	private void finishGame() {
		String lastPlayer = players.get(0);
		tempoDeJogo -= System.currentTimeMillis();
		finalizado = true;

		Player player = Bukkit.getPlayer(lastPlayer);
		
		clearContents(player);
		
		player.teleport(Fight.outLocation);
		
		StringBuilder sb = new StringBuilder();
		sb.append("�7�m-----------------------------\n\n");
		sb.append("�bJogo finalizado!\n");
		sb.append("�bGanhador: �a�l");
		sb.append(lastPlayer);
		sb.append("\n�bParab�ns! jogue novamente /pvp\n\n");
		sb.append("�7�m----------------------------\n");
		Bukkit.broadcastMessage(sb.toString());
		playSoundForLeaves(Sound.ENDERDRAGON_GROWL);
		
		arena.size = 0;
		time.unregister();
		
		Fight.clear();
	}

	
	
	private void clearContents(Player player) {
		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
	}

	public void sendMessageForAll(String msg) {
		players.forEach(s -> {
			Player player = Bukkit.getPlayer(s);
			if(player!=null) player.sendMessage(msg);
		});
	}
	
	public void playSoundForAll(Sound sound) {
		players.forEach(s -> {
			Player player = Bukkit.getPlayer(s);
			player.playSound(player.getLocation(), sound, 1F, 1F);
		});
	}
	

	public void playSoundForLeaves(Sound sound) {
		players.forEach(s -> {
			Player player = Bukkit.getPlayer(s);
			player.playSound(player.getLocation(), sound, 0.92F, 1F);
		});
	}
	
	public boolean isFinalizado() {
		return finalizado;
	}
	
	public boolean isIniciado() {
		return iniciado;
	}
	
	public boolean isPrepare() {
		return prepare;
	}
}
