package br.com.endcraft.fight;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Comandos implements CommandExecutor{
	
	private List<String> inCommand = new ArrayList<>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length >= 2 && sender.isOp() && args[0].equalsIgnoreCase("savekit")) {
			StringBuilder sb = new StringBuilder();
			for(int x = 1; x < args.length;x++) {
				sb.append(args[x]);
				sb.append(" ");
			}
			
			Kit kit = new Kit();
			
			Player player = (Player)sender;
			kit.setArmor(player.getInventory().getArmorContents());
			kit.setContents(player.getInventory().getContents());
			kit.setName(sb.toString());
			Fight.getKits().add(kit);
			player.sendMessage("�7[Ultra PVP] �a KIT salvo");
			return true;
		}
		if(args.length >= 1 && sender.isOp() && args[0].equalsIgnoreCase("arena")) {
			try {
				if(args[1].equalsIgnoreCase("create")) {
					String name = args[2];
					Arena arena = new Arena(name);
					Fight.getArenas().add(arena);
					sender.sendMessage("�7[Ultra PVP] �bArena criada! marque spawnpoints");
					return true;
				}
				if(args[1].equalsIgnoreCase("setspawn")) {
					Arena arena;
					if((arena = Fight.getArenaByName(args[2])) == null) {
						sender.sendMessage("�7[Ultra PVP] �cArena n�o encontrada");
						return true;
					}
					SpawnPoint sp = SpawnPoint.toSpawnPoint(((Player)sender).getLocation());
					arena.getSpawns().add(sp);
					sender.sendMessage("�7[Ultra PVP] �bSpawnPoint Adicionado");
					return true;
				}
				if(args[1].equalsIgnoreCase("resetspawn")) {
					Arena arena;
					if((arena = Fight.getArenaByName(args[2])) == null) {
						sender.sendMessage("�7[Ultra PVP] �cArena n�o encontrada");
						return true;
					}
					arena.getSpawns().clear();
					sender.sendMessage("�7[Ultra PVP] �cSpawnPoints foram resetados");
					return true;
				}
				if(args[1].equalsIgnoreCase("saida")) {
					Location loc = ((Player)sender).getLocation();
					Fight.getInstance().getConfig().set("saida", loc.getWorld().getName() + ";"
							+ loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ());
					Fight.outLocation = loc;
					Fight.getInstance().saveConfig();
					sender.sendMessage("�7[Ultra PVP] �cLocal padr�o de saida definido.");
					return true;
				}
			}catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("�cErro: " + e.getMessage());
				StringBuilder sb = new StringBuilder();
				sb.append("�a�m-------- �cComandos �a�m--------\n");
				sb.append("�b/pvp arena create <nome>\n");
				sb.append("�b/pvp arena setspawn <nome>\n");
				sb.append("�b/pvp arena resetspawn <nome>\n");
				sender.sendMessage(sb.toString());
				return true;
			}
		}
		if(args.length == 0 && sender instanceof Player) {
			if(!Tools.checkClearInventory((Player)sender)) {
				sender.sendMessage("�7[Ultra PVP] �cVoc� precisa limpar o invent�rio para participar do evento!");
				return true;
			}
			if(inCommand.contains(sender.getName())) {
				return true;
			}
			if(Fight.getGameByPlayer(sender.getName()) != null) {
				return true;
			}
			inCommand.add(sender.getName());
			entryLobby((Player)sender);
			sender.sendMessage("�7[Ultra PVP] Objetivo do jogo: Mate todos os jogadores!");
		} else if(args.length == 1 && sender instanceof Player) {
			if(args[0].equalsIgnoreCase("sair")) {
				Game game;
				if((game = Fight.getGameByPlayer(sender.getName())) != null) {
					game.exitQuery((Player)sender);
				}
				return true;
			}
			sender.sendMessage("�bOpa? use somente /pvp ou /pvp sair <3");
		}
		return false;
	}

	private void entryLobby(Player sender) {
		for(Game games : Fight.getGames()) {
			if(games.getQuantidade() < Game.MAX_PLAYERS) {
				games.entryQuery(sender);
				inCommand.remove(sender.getName());
				Fight.log("Jogador entrou size do jogo: " + games.getQuantidade());
				return;
			}
		}
		// Se n�o encontrar uma sala, vamos criar outra.
		createGame(sender);
		
	}

	private void createGame(Player player) {
		for(Arena arena : Fight.getArenas()) {
			if(arena.size == 0 && !arena.spawns.isEmpty()) {
				Game game = new Game(Fight.getInstance(), arena);
				Fight.getGames().add(game);
				game.entryQuery(player);
				inCommand.remove(player.getName());
				return;
			}
		}
		player.sendMessage("�7[Ultra PVP] �aBuscando arenas livres, aguarde...");
		new BukkitRunnable() {
			@Override
			public void run() {
				entryLobby(player);
			}
		}.runTaskLater(Fight.getInstance(), 20 * 10);
		
	}

	
}
